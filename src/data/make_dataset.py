# -*- coding: utf-8 -*-
import os
import logging

import click
import librosa
import numpy as np
import pandas as pd
from tqdm import tqdm


EMPTY_AUDIO_FILE_SIZE = 44


@click.command()
@click.argument("input_audio_path", type=click.Path(exists=True))
@click.argument("labels_filepath", type=click.Path(exists=True))
@click.argument("output_filepath", type=click.Path())
def process_input_audio(
    input_audio_path: str, labels_filepath: str, output_filepath: str
) -> None:
    """Runs data processing scripts to turn raw data from (../raw) into
    cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)

    # 1 - loading labels and adding merge column
    df = pd.read_csv(labels_filepath)
    df["Name"] = df[df.columns[0:3]].apply(
        lambda x: "_".join(x.dropna().astype(str)), axis=1
    )

    # 2 - removing empty audio files
    ignore_set = set()
    for filename in os.listdir(input_audio_path):
        input_filepath = os.path.join(input_audio_path, filename)
        if "FluencyBank" not in filename and filename.endswith(".wav"):
            if os.stat(input_filepath).st_size == EMPTY_AUDIO_FILE_SIZE:
                ignore_set.add(filename)
                filename = filename[:-4]
                df = df[df.Name != filename]

    # 3 - extracting mfcc features from audio files
    features = {}
    for filename in tqdm(os.listdir(input_audio_path)):
        if "FluencyBank" not in filename and filename not in ignore_set:
            input_wav_filepath = os.path.join(input_audio_path, filename)
            audio, sample_rate = librosa.load(
                input_wav_filepath, res_type="kaiser_fast", sr=None
            )
            mfccs = np.mean(
                librosa.feature.mfcc(y=audio, sr=sample_rate, n_mfcc=13).T, axis=0
            )
            features[filename[:-4]] = mfccs
    df_features = pd.DataFrame.from_dict(features)
    df_features = df_features.transpose().reset_index().sort_values(by="index")
    df_features.rename(columns={"index": "Name"}, inplace=True)
    df_final = pd.merge(df, df_features, how="inner", on="Name")

    df_final.to_csv(output_filepath, index=False)
    logger.info(f"Processed final data set from raw data, shape {df_final.shape}")
